import { container } from 'tsyringe';

import './providers';
import ActorsRepository from '@modules/actors/infra/typeorm/repositories/ActorsRepository';
import IActorsRepository from '@modules/actors/repositories/IActorsRepository';
import DirectorsRepository from '@modules/directors/infra/typeorm/repositories/DirectorsRepository';
import IDirectorsRepository from '@modules/directors/repositories/IDirectorsRepository';
import MovieActorsRepository from '@modules/movie_actors/infra/typeorm/repositories/MovieActorsRepository';
import IMovieActorsRepository from '@modules/movie_actors/repositories/IMovieActorsRepository';
import MovieDirectorsRepository from '@modules/movie_directors/infra/typeorm/repositories/MovieDirectorsRepository';
import IMovieDirectorsRepository from '@modules/movie_directors/repositories/IMovieDirectorsRepository';
import MovieGenresRepository from '@modules/movie_genres/infra/typeorm/repositories/MovieGenresRepository';
import IMovieGenresRepository from '@modules/movie_genres/repositories/IMovieGenresRepository';
import MovieRatingsRepository from '@modules/movie_ratings/infra/typeorm/repositories/MovieRatingsRepository';
import IMovieRatingsRepository from '@modules/movie_ratings/repositories/IMovieRatingsRepository';
import MoviesRepository from '@modules/movies/infra/typeorm/repositories/MoviesRepository';
import IMoviesRepository from '@modules/movies/repositories/IMoviesRepository';
import UsersRepository from '@modules/users/infra/typeorm/repositories/UsersRepository';
import IUsersRepository from '@modules/users/repositories/IUsersRepository';

container.registerSingleton<IUsersRepository>(
  'UsersRepository',
  UsersRepository,
);

container.registerSingleton<IMoviesRepository>(
  'MoviesRepository',
  MoviesRepository,
);

container.registerSingleton<IActorsRepository>(
  'ActorsRepository',
  ActorsRepository,
);

container.registerSingleton<IDirectorsRepository>(
  'DirectorsRepository',
  DirectorsRepository,
);

container.registerSingleton<IMovieActorsRepository>(
  'MovieActorsRepository',
  MovieActorsRepository,
);

container.registerSingleton<IMovieDirectorsRepository>(
  'MovieDirectorsRepository',
  MovieDirectorsRepository,
);

container.registerSingleton<IMovieGenresRepository>(
  'MovieGenresRepository',
  MovieGenresRepository,
);

container.registerSingleton<IMovieRatingsRepository>(
  'MovieRatingsRepository',
  MovieRatingsRepository,
);
