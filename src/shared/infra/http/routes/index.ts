import { Router } from 'express';

import movieRatingsRouter from '@modules/movie_ratings/infra/http/routes/movies_ratings.routes';
import moviesRouter from '@modules/movies/infra/http/routes/movies.routes';
import sessionsRouter from '@modules/users/infra/http/routes/sessions.routes';
import usersRouter from '@modules/users/infra/http/routes/users.routes';

const routes = Router();

routes.use('/users', usersRouter);
routes.use('/sessions', sessionsRouter);
routes.use('/movies', moviesRouter);
routes.use('/movie-ratings', movieRatingsRouter);

routes.get('/', (_request, response) =>
  response.json({
    name: 'IMDb API',
    version: '1.0.0',
  }),
);

export default routes;
