import ICreateMovieRatingDTO from '../dtos/ICreateMovieRatingDTO';
import MovieRating from '../infra/typeorm/entities/MovieRating';

export default interface IMovieRatingsRepository {
  findAll(): Promise<MovieRating[] | undefined>;
  findById(id: string): Promise<MovieRating | undefined>;
  findByMovieId(movie_id: string): Promise<MovieRating[] | undefined>;
  findByUserAndMovieId({
    user_id,
    movie_id,
  }: {
    user_id: string;
    movie_id: string;
  }): Promise<MovieRating | undefined>;
  create(data: ICreateMovieRatingDTO): Promise<MovieRating>;
  save(movie_genre: MovieRating): Promise<MovieRating>;
}
