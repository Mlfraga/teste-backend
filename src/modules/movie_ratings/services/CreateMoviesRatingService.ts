import { classToClass } from 'class-transformer';
import { injectable, inject } from 'tsyringe';

import AppError from '@shared/errors/AppError';

import IMoviesRepository from '@modules/movies/repositories/IMoviesRepository';
import IUsersRepository from '@modules/users/repositories/IUsersRepository';

import MovieRating from '../infra/typeorm/entities/MovieRating';
import IMovieRatingsRepository from '../repositories/IMovieRatingsRepository';

interface IRequest {
  rate: number;
  movie_id: string;
  user_id: string;
}

@injectable()
class CreateMoviesRatingService {
  constructor(
    @inject('MoviesRepository')
    private moviesRepository: IMoviesRepository,

    @inject('MovieRatingsRepository')
    private movieRatingsRepository: IMovieRatingsRepository,

    @inject('UsersRepository')
    private usersRepository: IUsersRepository,
  ) {}

  public async execute({
    rate,
    movie_id,
    user_id,
  }: IRequest): Promise<MovieRating> {
    const movie = await this.moviesRepository.findById(movie_id);

    if (!movie) {
      throw new AppError('This movie was not found', 404);
    }

    const user = await this.usersRepository.findById(user_id);

    if (!user) {
      throw new AppError('This user was not found', 404);
    }

    const ratingByMovieAndUser =
      await this.movieRatingsRepository.findByUserAndMovieId({
        user_id,
        movie_id,
      });

    if (ratingByMovieAndUser) {
      await this.movieRatingsRepository.save({ ...ratingByMovieAndUser, rate });

      return classToClass({ ...ratingByMovieAndUser, rate });
    }

    const createdRating = await this.movieRatingsRepository.create({
      movie_id,
      user_id,
      rate,
    });

    return classToClass(createdRating);
  }
}

export default CreateMoviesRatingService;
