export default interface ICreateMovieRatingDTO {
  movie_id: string;
  user_id: string;
  rate: number;
}
