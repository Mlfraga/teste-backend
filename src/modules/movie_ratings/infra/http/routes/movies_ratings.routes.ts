import { celebrate, Segments, Joi } from 'celebrate';
import { Router } from 'express';

import MoviesRatingsController from '../controllers/MoviesRatingsController';
import ensureAuthenticated from '../middlewares/ensureAuthenticated';
import ensureUserIsNotAdmin from '../middlewares/ensureUserIsNotAdmin';

const movieRatingsRouter = Router();
const moviesRatingsController = new MoviesRatingsController();

movieRatingsRouter.post(
  '/:movie_id',
  celebrate({
    [Segments.BODY]: {
      rate: Joi.number().min(1).max(4).required(),
    },
    [Segments.PARAMS]: {
      movie_id: Joi.string().uuid().required(),
    },
  }),
  ensureAuthenticated,
  ensureUserIsNotAdmin,
  moviesRatingsController.create,
);

export default movieRatingsRouter;
