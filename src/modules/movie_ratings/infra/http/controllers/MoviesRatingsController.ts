import { Request, Response } from 'express';
import { container } from 'tsyringe';

import CreateMoviesRatingService from '@modules/movie_ratings/services/CreateMoviesRatingService';

export default class MoviesRatingsController {
  public async create(request: Request, response: Response): Promise<Response> {
    const { rate } = request.body;
    const { movie_id } = request.params;
    const { id: user_id } = request.user;

    const createMoviesRatingService = container.resolve(
      CreateMoviesRatingService,
    );

    const createdRatingMovie = await createMoviesRatingService.execute({
      rate,
      movie_id,
      user_id,
    });

    return response.json(createdRatingMovie);
  }
}
