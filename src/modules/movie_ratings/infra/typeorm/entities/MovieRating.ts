import { Exclude } from 'class-transformer';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';

import Movie from '@modules/movies/infra/typeorm/entities/Movie';
import User from '@modules/users/infra/typeorm/entities/User';

@Entity('movie_ratings')
export default class MovieRating {
  @PrimaryGeneratedColumn('uuid')
  @Exclude()
  id: string;

  @Column()
  @Exclude()
  movie_id: string;

  @ManyToOne(() => Movie, movie => movie.genres)
  @JoinColumn({ name: 'movie_id' })
  movie: Movie;

  @Column()
  @Exclude()
  user_id: string;

  @ManyToOne(() => User, user => user.ratings)
  @JoinColumn({ name: 'user_id' })
  user: User;

  @Column()
  rate: number;

  @CreateDateColumn()
  @Exclude()
  created_at: Date;

  @CreateDateColumn()
  @Exclude()
  updated_at: Date;
}
