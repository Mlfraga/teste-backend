import { getRepository, Repository } from 'typeorm';

import ICreateMovieRatingDTO from '@modules/movie_ratings/dtos/ICreateMovieRatingDTO';
import IMovieRatingsRepository from '@modules/movie_ratings/repositories/IMovieRatingsRepository';

import MovieRating from '../entities/MovieRating';

class MovieRatingsRepository implements IMovieRatingsRepository {
  private ormRepository: Repository<MovieRating>;

  constructor() {
    this.ormRepository = getRepository(MovieRating);
  }

  public async findAll(): Promise<MovieRating[] | undefined> {
    const ratings = await this.ormRepository.find({
      relations: ['movie', 'user'],
    });

    return ratings;
  }

  public async findByMovieId(
    movie_id: string,
  ): Promise<MovieRating[] | undefined> {
    const ratings = await this.ormRepository.find({
      where: {
        movie_id,
      },
      relations: ['movie', 'user'],
    });

    return ratings;
  }

  public async findByUserId(
    user_id: string,
  ): Promise<MovieRating[] | undefined> {
    const user_ratings = await this.ormRepository.find({
      where: {
        user_id,
      },
      relations: ['movie', 'user'],
    });

    return user_ratings;
  }

  public async findById(id: string): Promise<MovieRating | undefined> {
    const rating = await this.ormRepository.findOne(id);

    return rating;
  }

  public async findByUserAndMovieId({
    movie_id,
    user_id,
  }: {
    movie_id: string;
    user_id: string;
  }): Promise<MovieRating | undefined> {
    const rating = await this.ormRepository.findOne({
      where: {
        movie_id,
        user_id,
      },
    });

    return rating;
  }

  public async create(data: ICreateMovieRatingDTO): Promise<MovieRating> {
    const rating = this.ormRepository.create(data);

    await this.ormRepository.save(rating);

    return rating;
  }

  public async save(movie_genre: MovieRating): Promise<MovieRating> {
    return this.ormRepository.save(movie_genre);
  }
}

export default MovieRatingsRepository;
