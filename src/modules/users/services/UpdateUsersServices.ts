import { injectable, inject } from 'tsyringe';

import IHashProvider from '@shared/container/providers/HashProvider/models/IHashProvider';
import AppError from '@shared/errors/AppError';

import IUsersRepository from '../repositories/IUsersRepository';

interface IRequest {
  id: string;
  request_author: string;
  name: string;
  email: string;
  password: string;
}

interface IResponse {
  success: boolean;
}

@injectable()
class UpdateUsersServices {
  constructor(
    @inject('UsersRepository')
    private usersRepository: IUsersRepository,

    @inject('HashProvider')
    private hashProvider: IHashProvider,
  ) {}

  public async execute({
    id,
    name,
    email,
    password,
    request_author,
  }: IRequest): Promise<IResponse> {
    const user = await this.usersRepository.findById(id);

    if (!user) {
      throw new AppError('This user does not exist.', 404);
    }

    if (user.id !== request_author) {
      throw new AppError('This user does not match request author.', 401);
    }

    if (!user.enabled) {
      throw new AppError('This user is disabled.', 401);
    }

    if (email) {
      const userByEmail = await this.usersRepository.findByEmail(email);

      if (userByEmail) {
        throw new AppError('This email already exists.');
      }
    }

    const hasshedPassword =
      password && (await this.hashProvider.generateHash(password));

    this.usersRepository.save({
      ...user,
      ...(name && { name }),
      ...(email && { email }),
      ...(password && { password: hasshedPassword }),
    });

    return {
      success: true,
    };
  }
}

export default UpdateUsersServices;
