import { injectable, inject } from 'tsyringe';

import AppError from '@shared/errors/AppError';

import IUsersRepository from '../repositories/IUsersRepository';

interface IRequest {
  id: string;
  request_author: string;
}

interface IResponse {
  success: boolean;
}

@injectable()
class DisableUsersServices {
  constructor(
    @inject('UsersRepository')
    private usersRepository: IUsersRepository,
  ) {}

  public async execute({ id, request_author }: IRequest): Promise<IResponse> {
    const user = await this.usersRepository.findById(id);

    if (!user) {
      throw new AppError('This user does not exist.');
    }

    if (user.id !== request_author) {
      throw new AppError('This user does not match request author.', 401);
    }

    this.usersRepository.save({ ...user, enabled: false });

    return {
      success: true,
    };
  }
}

export default DisableUsersServices;
