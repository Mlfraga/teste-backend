import { classToClass } from 'class-transformer';
import { injectable, inject } from 'tsyringe';

import IHashProvider from '@shared/container/providers/HashProvider/models/IHashProvider';
import AppError from '@shared/errors/AppError';

import User from '../infra/typeorm/entities/User';
import IUsersRepository from '../repositories/IUsersRepository';

interface IRequest {
  name: string;
  email: string;
  password: string;
  admin_registration_key: string;
}

@injectable()
class CreateUserService {
  constructor(
    @inject('UsersRepository')
    private usersRepository: IUsersRepository,

    @inject('HashProvider')
    private hashProvider: IHashProvider,
  ) {}

  public async execute({
    name,
    email,
    password,
    admin_registration_key,
  }: IRequest): Promise<User> {
    const hashedPassword = await this.hashProvider.generateHash(password);

    if (admin_registration_key) {
      if (admin_registration_key !== process.env.ADMIN_REGISTRATION_KEY) {
        throw new AppError('admin_registration_key incorrect.');
      }

      const checkEmailExists = await this.usersRepository.findByEmail(email);

      if (checkEmailExists) {
        if (!checkEmailExists.enabled) {
          const reactivatedUser = await this.usersRepository.save({
            ...checkEmailExists,
            enabled: true,
            password: hashedPassword,
            admin: true,
          });

          return classToClass(reactivatedUser);
        }

        throw new AppError('Email address already used.');
      }
      const user = await this.usersRepository.create({
        name,
        email,
        password: hashedPassword,
        admin: true,
      });

      return classToClass(user);
    }

    const checkEmailExists = await this.usersRepository.findByEmail(email);

    if (checkEmailExists) {
      if (!checkEmailExists.enabled) {
        const reactivatedUser = await this.usersRepository.save({
          ...checkEmailExists,
          enabled: true,
          password: hashedPassword,
          admin: false,
        });

        return classToClass(reactivatedUser);
      }

      throw new AppError('Email address already used.');
    }

    const user = await this.usersRepository.create({
      name,
      email,
      password: hashedPassword,
      admin: false,
    });

    return classToClass(user);
  }
}

export default CreateUserService;
