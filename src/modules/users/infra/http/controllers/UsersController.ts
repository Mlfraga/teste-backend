import { Request, Response } from 'express';
import { container } from 'tsyringe';

import CreateUserService from '@modules/users/services/CreateUsersServices';
import DisableUsersServices from '@modules/users/services/DisableUsersServices';
import UpdateUsersServices from '@modules/users/services/UpdateUsersServices';

export default class UsersController {
  public async create(request: Request, response: Response): Promise<Response> {
    const { name, email, password, admin_registration_key } = request.body;

    const createUserService = container.resolve(CreateUserService);

    const user = await createUserService.execute({
      name,
      email,
      password,
      admin_registration_key,
    });

    return response.json(user);
  }

  public async delete(request: Request, response: Response): Promise<Response> {
    const { id } = request.params;

    const request_author = request.user.id;

    const disableUsersServices = container.resolve(DisableUsersServices);

    const disable_return = await disableUsersServices.execute({
      id,
      request_author,
    });

    return response.json(disable_return);
  }

  public async update(request: Request, response: Response): Promise<Response> {
    const { name, email, password } = request.body;
    const { id } = request.params;

    const request_author = request.user.id;

    const updateUsersServices = container.resolve(UpdateUsersServices);

    const update_return = await updateUsersServices.execute({
      name,
      email,
      password,
      id,
      request_author,
    });

    return response.json(update_return);
  }
}
