import { Exclude } from 'class-transformer';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  OneToMany,
} from 'typeorm';

import MovieRating from '@modules/movie_ratings/infra/typeorm/entities/MovieRating';

@Entity('users')
export default class Users {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column()
  email: string;

  @Column()
  @Exclude()
  password: string;

  @Column()
  admin: boolean;

  @Column()
  enabled: boolean;

  @OneToMany(() => MovieRating, movie_rating => movie_rating.user)
  ratings: MovieRating[];

  @CreateDateColumn()
  @Exclude()
  created_at: Date;

  @CreateDateColumn()
  @Exclude()
  updated_at: Date;
}
