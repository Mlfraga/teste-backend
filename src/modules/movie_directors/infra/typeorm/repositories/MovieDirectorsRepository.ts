import { getRepository, Repository } from 'typeorm';

import ICreateMovieDirectorDTO from '@modules/movie_directors/dtos/ICreateMovieDirectorDTO';
import IMovieDirectorsRepository from '@modules/movie_directors/repositories/IMovieDirectorsRepository';

import MovieDirector from '../entities/MovieDirector';

class MovieDirectorsRepository implements IMovieDirectorsRepository {
  private ormRepository: Repository<MovieDirector>;

  constructor() {
    this.ormRepository = getRepository(MovieDirector);
  }

  public async findAll(): Promise<MovieDirector[] | undefined> {
    const movie_directors = await this.ormRepository.find({
      relations: ['director', 'movie'],
    });

    return movie_directors;
  }

  public async findByMovieId(
    movie_id: string,
  ): Promise<MovieDirector[] | undefined> {
    const movie_directors = await this.ormRepository.find({
      where: {
        movie_id,
      },
      relations: ['director', 'movie'],
    });

    return movie_directors;
  }

  public async findByDirectorId(
    director_id: string,
  ): Promise<MovieDirector[] | undefined> {
    const movies_director = await this.ormRepository.find({
      where: {
        director_id,
      },
      relations: ['director', 'movie'],
    });

    return movies_director;
  }

  public async findById(id: string): Promise<MovieDirector | undefined> {
    const movie_director = await this.ormRepository.findOne(id);

    return movie_director;
  }

  public async create(data: ICreateMovieDirectorDTO): Promise<MovieDirector> {
    const movie_director = this.ormRepository.create(data);

    await this.ormRepository.save(movie_director);

    return movie_director;
  }

  public async save(movie_director: MovieDirector): Promise<MovieDirector> {
    return this.ormRepository.save(movie_director);
  }
}

export default MovieDirectorsRepository;
