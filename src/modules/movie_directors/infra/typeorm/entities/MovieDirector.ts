import { Exclude } from 'class-transformer';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';

import Director from '@modules/directors/infra/typeorm/entities/Director';
import Movie from '@modules/movies/infra/typeorm/entities/Movie';

@Entity('movie_directors')
export default class MovieDirector {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  @Exclude()
  movie_id: string;

  @ManyToOne(() => Movie, movie => movie.directors)
  @JoinColumn({ name: 'movie_id' })
  movie: Movie;

  @Column()
  @Exclude()
  director_id: string;

  @ManyToOne(() => Director, director => director.movies)
  @JoinColumn({ name: 'director_id' })
  director: Director;

  @CreateDateColumn()
  @Exclude()
  created_at: Date;

  @CreateDateColumn()
  @Exclude()
  updated_at: Date;
}
