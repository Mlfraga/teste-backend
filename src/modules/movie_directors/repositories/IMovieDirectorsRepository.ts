import ICreateMovieDirectorDTO from '../dtos/ICreateMovieDirectorDTO';
import MovieDirector from '../infra/typeorm/entities/MovieDirector';

export default interface IMovieDirectorsRepository {
  findAll(): Promise<MovieDirector[] | undefined>;
  findById(id: string): Promise<MovieDirector | undefined>;
  findByMovieId(movie_id: string): Promise<MovieDirector[] | undefined>;
  findByDirectorId(director_id: string): Promise<MovieDirector[] | undefined>;
  create(data: ICreateMovieDirectorDTO): Promise<MovieDirector>;
  save(MovieDirector: MovieDirector): Promise<MovieDirector>;
}
