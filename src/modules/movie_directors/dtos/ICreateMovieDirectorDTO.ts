export default interface ICreateMovieDirectorDTO {
  movie_id: string;
  director_id: string;
}
