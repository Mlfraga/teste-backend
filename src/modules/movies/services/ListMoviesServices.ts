import { injectable, inject } from 'tsyringe';

import Movie from '../infra/typeorm/entities/Movie';
import IMoviesRepository from '../repositories/IMoviesRepository';

interface IRequest {
  director?: string;
  name?: string;
  genre?: string;
  actor?: string;
}

@injectable()
class ListMoviesServices {
  constructor(
    @inject('MoviesRepository')
    private moviesRepository: IMoviesRepository,
  ) {}

  public async execute({
    director,
    name,
    genre,
    actor,
  }: IRequest): Promise<Movie[] | undefined> {
    const movies = await this.moviesRepository.findAll({
      genre,
      name,
      director,
      actor,
    });

    return movies;
  }
}

export default ListMoviesServices;
