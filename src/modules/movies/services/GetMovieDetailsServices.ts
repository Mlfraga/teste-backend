import { classToClass } from 'class-transformer';
import { injectable, inject } from 'tsyringe';

import AppError from '@shared/errors/AppError';

import IMovieRatingsRepository from '@modules/movie_ratings/repositories/IMovieRatingsRepository';

import IMoviesRepository from '../repositories/IMoviesRepository';

interface IResponse {
  rating: number;
  id: string;
  name: string;
  creator: string;
  plot_lines: string;
  synopsis: string;
  category: string;
  release_date: Date;
  genres: Array<{
    genre: string;
  }>;
  actors: Array<{
    role: string;
    actor: {
      id: string;
      name: string;
    };
  }>;
  directors: Array<{
    id: string;
    director: {
      id: string;
      name: string;
    };
  }>;
}

@injectable()
class GetMovieDetailsServices {
  constructor(
    @inject('MoviesRepository')
    private moviesRepository: IMoviesRepository,

    @inject('MovieRatingsRepository')
    private movieRatingsRepository: IMovieRatingsRepository,
  ) {}

  public async execute(id: string): Promise<IResponse | undefined> {
    const movie = await this.moviesRepository.findById(id);

    if (!movie) {
      throw new AppError('Movie not found', 404);
    }

    const ratings = await this.movieRatingsRepository.findByMovieId(movie.id);

    let sum_of_ratings = 0;
    let total_rating = 0;

    if (ratings) {
      ratings.forEach(rating => {
        sum_of_ratings = Number(rating.rate) + Number(sum_of_ratings);
      });

      if (sum_of_ratings === 0) {
        total_rating = 0;
      } else {
        total_rating = sum_of_ratings / ratings.length;
      }
    }

    return { rating: Number(total_rating.toFixed(2)), ...classToClass(movie) };
  }
}

export default GetMovieDetailsServices;
