import { classToClass } from 'class-transformer';
import { injectable, inject } from 'tsyringe';

import AppError from '@shared/errors/AppError';

import IActorsRepository from '@modules/actors/repositories/IActorsRepository';
import IDirectorsRepository from '@modules/directors/repositories/IDirectorsRepository';
import MovieActor from '@modules/movie_actors/infra/typeorm/entities/MovieActor';
import IMovieActorsRepository from '@modules/movie_actors/repositories/IMovieActorsRepository';
import MovieDirector from '@modules/movie_directors/infra/typeorm/entities/MovieDirector';
import IMovieDirectorsRepository from '@modules/movie_directors/repositories/IMovieDirectorsRepository';
import MovieGenre from '@modules/movie_genres/infra/typeorm/entities/MovieGenre';
import IMovieGenresRepository from '@modules/movie_genres/repositories/IMovieGenresRepository';

import Movie from '../infra/typeorm/entities/Movie';
import IMoviesRepository from '../repositories/IMoviesRepository';

interface IRequest {
  name: string;
  creator: string;
  plot_lines: string;
  synopsis: string;
  category: string;
  release_date: Date;
  actors: Array<{
    name: string;
    role: string;
  }>;
  directors: Array<string>;
  genres: Array<string>;
}

@injectable()
class CreateMovieService {
  constructor(
    @inject('MoviesRepository')
    private moviesRepository: IMoviesRepository,

    @inject('ActorsRepository')
    private actorsRepository: IActorsRepository,

    @inject('DirectorsRepository')
    private directorsRepository: IDirectorsRepository,

    @inject('MovieDirectorsRepository')
    private movieDirectorsRepository: IMovieDirectorsRepository,

    @inject('MovieActorsRepository')
    private movieActorsRepository: IMovieActorsRepository,

    @inject('MovieGenresRepository')
    private movieGenresRepository: IMovieGenresRepository,
  ) {}

  public async execute({
    name,
    creator,
    plot_lines,
    synopsis,
    category,
    release_date,
    actors,
    directors,
    genres,
  }: IRequest): Promise<Movie> {
    const movieByName = await this.moviesRepository.findByName(name);

    if (movieByName) {
      throw new AppError('This movie already exists.', 409);
    }

    const createdMovie = await this.moviesRepository.create({
      name,
      creator,
      plot_lines,
      synopsis,
      category,
      release_date,
    });

    const movieActorsPromises: Promise<MovieActor>[] = actors.map(
      async actor => {
        const actorByName = await this.actorsRepository.findByName(actor.name);

        if (!actorByName) {
          const createdActor = await this.actorsRepository.create({
            name: actor.name,
          });

          const createdActorMovie = await this.movieActorsRepository.create({
            actor_id: createdActor.id,
            movie_id: createdMovie.id,
            role: actor.role,
          });

          return createdActorMovie;
        }

        const createdActorMovie = await this.movieActorsRepository.create({
          actor_id: actorByName.id,
          movie_id: createdMovie.id,
          role: actor.role,
        });

        return createdActorMovie;
      },
    );

    const directorsMoviePromises: Promise<MovieDirector>[] = directors.map(
      async director => {
        const directorByName = await this.directorsRepository.findByName(
          director,
        );

        if (!directorByName) {
          const createdDirector = await this.directorsRepository.create({
            name: director,
          });

          const createdDirectorMovie =
            await this.movieDirectorsRepository.create({
              director_id: createdDirector.id,
              movie_id: createdMovie.id,
            });

          return createdDirectorMovie;
        }

        const createdDirectorMovie = await this.movieDirectorsRepository.create(
          {
            director_id: directorByName.id,
            movie_id: createdMovie.id,
          },
        );

        return createdDirectorMovie;
      },
    );

    const genresMoviePromises: Promise<MovieGenre>[] = genres.map(
      async genre => {
        const createdGenre = await this.movieGenresRepository.create({
          genre,
          movie_id: createdMovie.id,
        });

        return createdGenre;
      },
    );

    await Promise.all<MovieActor>(movieActorsPromises);
    await Promise.all<MovieDirector>(directorsMoviePromises);
    await Promise.all<MovieGenre>(genresMoviePromises);

    return classToClass(createdMovie);
  }
}

export default CreateMovieService;
