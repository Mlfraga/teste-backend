import ICreateMovieDTO from '../dtos/ICreateMovieDTO';
import Movie from '../infra/typeorm/entities/Movie';

export interface IMoviesFilters {
  director?: string;
  name?: string;
  genre?: string;
  actor?: string;
}

export default interface IMoviesRepository {
  findAll({
    director,
    name,
    genre,
    actor,
  }: IMoviesFilters): Promise<Movie[] | undefined>;
  findById(id: string): Promise<Movie | undefined>;
  findByName(name: string): Promise<Movie | undefined>;
  create(data: ICreateMovieDTO): Promise<Movie>;
  save(movie: Movie): Promise<Movie>;
}
