import { celebrate, Segments, Joi } from 'celebrate';
import { Router } from 'express';

import MoviesController from '../controllers/MoviesController';
import ensureAuthenticated from '../middlewares/ensureAuthenticated';
import ensureUserIsAdmin from '../middlewares/ensureUserIsAdmin';

const moviesRouter = Router();
const moviesController = new MoviesController();

moviesRouter.post(
  '/',
  celebrate({
    [Segments.BODY]: {
      name: Joi.string().required(),
      creator: Joi.string().required(),
      plot_lines: Joi.string(),
      synopsis: Joi.string().required(),
      category: Joi.string().required(),
      release_date: Joi.date().required(),
      actors: Joi.array().items(
        Joi.object()
          .keys({
            name: Joi.string().required(),
            role: Joi.string().required(),
          })
          .required(),
      ),
      directors: Joi.array().items(Joi.string().required()),
      genres: Joi.array().items(Joi.string().required()),
    },
  }),
  ensureAuthenticated,
  ensureUserIsAdmin,
  moviesController.create,
);

moviesRouter.get(
  '/',
  celebrate({
    [Segments.QUERY]: {
      director: Joi.string(),
      name: Joi.string(),
      genre: Joi.string(),
      actor: Joi.string(),
    },
  }),
  ensureAuthenticated,
  moviesController.index,
);

moviesRouter.get(
  '/:id',
  celebrate({
    [Segments.PARAMS]: {
      id: Joi.string().uuid().required(),
    },
  }),
  ensureAuthenticated,
  moviesController.show,
);

export default moviesRouter;
