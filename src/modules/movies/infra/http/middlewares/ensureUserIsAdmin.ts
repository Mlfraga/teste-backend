import { Request, Response, NextFunction } from 'express';
import { container } from 'tsyringe';

import AppError from '@shared/errors/AppError';

import UsersRepository from '@modules/users/infra/typeorm/repositories/UsersRepository';

interface ITokenPayload {
  iat: number;
  exp: number;
  sub: string;
}

export default async function ensureUserIsAdmin(
  request: Request,
  response: Response,
  next: NextFunction,
): Promise<void> {
  const user_id = request.user.id;
  const usersRepository = container.resolve(UsersRepository);

  const user = await usersRepository.findById(user_id);

  if (!user || !user?.admin) {
    throw new AppError('This user does not have permission.', 405);
  }

  return next();
}
