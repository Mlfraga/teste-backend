import { Request, Response } from 'express';
import { container } from 'tsyringe';

import CreateMoviesServices from '@modules/movies/services/CreateMoviesServices';
import GetMovieDetailsServices from '@modules/movies/services/GetMovieDetailsServices';
import ListMoviesServices from '@modules/movies/services/ListMoviesServices';

export default class MoviesController {
  public async create(request: Request, response: Response): Promise<Response> {
    const {
      name,
      creator,
      plot_lines,
      synopsis,
      category,
      release_date,
      actors,
      directors,
      genres,
    } = request.body;

    const createMoviesService = container.resolve(CreateMoviesServices);

    const createdMovie = await createMoviesService.execute({
      name,
      creator,
      plot_lines,
      synopsis,
      category,
      release_date,
      actors,
      directors,
      genres,
    });

    return response.json(createdMovie);
  }

  public async index(request: Request, response: Response): Promise<Response> {
    const { director, name, genre, actor } = request.query;

    const listMoviesServices = container.resolve(ListMoviesServices);

    const movies = await listMoviesServices.execute({
      director: director && String(director),
      name: name && String(name),
      genre: genre && String(genre),
      actor: actor && String(actor),
    });

    return response.json(movies);
  }

  public async show(request: Request, response: Response): Promise<Response> {
    const { id: movie_id } = request.params;

    const getMovieDetailsServices = container.resolve(GetMovieDetailsServices);

    const movies = await getMovieDetailsServices.execute(movie_id);

    return response.json(movies);
  }
}
