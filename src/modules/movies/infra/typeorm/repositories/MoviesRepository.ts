import { getRepository, Repository } from 'typeorm';

import ICreateMovieDTO from '@modules/movies/dtos/ICreateMovieDTO';
import IMoviesRepository, {
  IMoviesFilters,
} from '@modules/movies/repositories/IMoviesRepository';

import Movie from '../entities/Movie';

class MoviesRepository implements IMoviesRepository {
  private ormRepository: Repository<Movie>;

  constructor() {
    this.ormRepository = getRepository(Movie);
  }

  public async findAll({
    director,
    name,
    genre,
    actor,
  }: IMoviesFilters): Promise<Movie[] | undefined> {
    let query = '';

    if (genre) {
      query = `genre.genre = '${genre}'`;
    }

    if (director) {
      if (query.length > 0) {
        query += ` AND director_info.name = '${director}'`;
      } else {
        query = `director_info.name = '${director}'`;
      }
    }

    if (name) {
      if (query.length > 0) {
        query += ` AND movies.name = '${name}'`;
      } else {
        query = `movies.name = '${name}' `;
      }
    }

    if (actor) {
      if (query.length > 0) {
        query += ` AND actor_info.name = '${actor}'`;
      } else {
        query += `actor_info.name = '${actor}'`;
      }
    }

    const movies = await this.ormRepository
      .createQueryBuilder('movies')
      .leftJoinAndSelect('movies.genres', 'genre')
      .leftJoinAndSelect('movies.actors', 'actor')
      .leftJoinAndSelect('actor.actor', 'actor_info')
      .leftJoinAndSelect('movies.directors', 'director')
      .leftJoinAndSelect('director.director', 'director_info')
      .where(query)
      .getMany();

    return movies;
  }

  public async findById(id: string): Promise<Movie | undefined> {
    const movie = await this.ormRepository.findOne({
      where: {
        id,
      },
      relations: [
        'genres',
        'actors',
        'actors.actor',
        'directors',
        'directors.director',
      ],
    });

    return movie;
  }

  public async findByName(name: string): Promise<Movie | undefined> {
    const movie = await this.ormRepository.findOne({
      where: {
        name,
      },
      relations: [
        'genres',
        'actors',
        'actors.actor',
        'directors',
        'directors.director',
      ],
    });

    return movie;
  }

  public async create(data: ICreateMovieDTO): Promise<Movie> {
    const movie = this.ormRepository.create(data);

    await this.ormRepository.save(movie);

    return movie;
  }

  public async save(movie: Movie): Promise<Movie> {
    return this.ormRepository.save(movie);
  }
}

export default MoviesRepository;
