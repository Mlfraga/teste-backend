import { Exclude } from 'class-transformer';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  OneToMany,
} from 'typeorm';

import MovieActor from '@modules/movie_actors/infra/typeorm/entities/MovieActor';
import MovieDirector from '@modules/movie_directors/infra/typeorm/entities/MovieDirector';
import MovieGenre from '@modules/movie_genres/infra/typeorm/entities/MovieGenre';

@Entity('movies')
export default class Movies {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column()
  creator: string;

  @Column()
  plot_lines: string;

  @Column()
  synopsis: string;

  @Column()
  category: string;

  @Column()
  release_date: Date;

  @OneToMany(() => MovieActor, movie_actor => movie_actor.movie)
  actors: MovieActor[];

  @OneToMany(() => MovieDirector, movie_director => movie_director.movie)
  directors: MovieDirector[];

  @OneToMany(() => MovieGenre, movie_genre => movie_genre.movie, {
    cascade: true,
    eager: true,
  })
  genres: MovieGenre[];

  @CreateDateColumn()
  @Exclude()
  created_at: Date;

  @CreateDateColumn()
  @Exclude()
  updated_at: Date;
}
