export default interface ICreateMovieDTO {
  name: string;
  creator: string;
  plot_lines: string;
  synopsis: string;
  category: string;
  release_date: Date;
}
