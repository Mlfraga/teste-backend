import ICreateMovieActorDTO from '../dtos/ICreateMovieActorDTO';
import MovieActor from '../infra/typeorm/entities/MovieActor';

export default interface IMovieActorsRepository {
  findAll(): Promise<MovieActor[] | undefined>;
  findById(id: string): Promise<MovieActor | undefined>;
  findByMovieId(movie_id: string): Promise<MovieActor[] | undefined>;
  findByActorId(actor_id: string): Promise<MovieActor[] | undefined>;
  create(data: ICreateMovieActorDTO): Promise<MovieActor>;
  save(movie_actor: MovieActor): Promise<MovieActor>;
}
