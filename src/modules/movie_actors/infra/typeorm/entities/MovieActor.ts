import { Exclude } from 'class-transformer';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  JoinColumn,
  ManyToOne,
} from 'typeorm';

import Actor from '@modules/actors/infra/typeorm/entities/Actor';
import Movie from '@modules/movies/infra/typeorm/entities/Movie';

@Entity('movie_actors')
export default class MovieActor {
  @PrimaryGeneratedColumn('uuid')
  @Exclude()
  id: string;

  @Column()
  role: string;

  @Column()
  @Exclude()
  movie_id: string;

  @ManyToOne(() => Movie, movie => movie.actors)
  @JoinColumn({ name: 'movie_id' })
  movie: Movie;

  @Column()
  @Exclude()
  actor_id: string;

  @ManyToOne(() => Actor, actor => actor.movies)
  @JoinColumn({ name: 'actor_id' })
  actor: Actor;

  @CreateDateColumn()
  @Exclude()
  created_at: Date;

  @CreateDateColumn()
  @Exclude()
  updated_at: Date;
}
