import { getRepository, Repository } from 'typeorm';

import ICreateMovieActorDTO from '@modules/movie_actors/dtos/ICreateMovieActorDTO';
import IMovieActorsRepository from '@modules/movie_actors/repositories/IMovieActorsRepository';

import MovieActor from '../entities/MovieActor';

class MovieActorsRepository implements IMovieActorsRepository {
  private ormRepository: Repository<MovieActor>;

  constructor() {
    this.ormRepository = getRepository(MovieActor);
  }

  public async findAll(): Promise<MovieActor[] | undefined> {
    const movie_actors = await this.ormRepository.find({
      relations: ['movies'],
    });

    return movie_actors;
  }

  public async findByMovieId(
    movie_id: string,
  ): Promise<MovieActor[] | undefined> {
    const movie_actors = await this.ormRepository.find({
      where: {
        moviesId: movie_id,
      },
    });

    return movie_actors;
  }

  public async findByActorId(
    actor_id: string,
  ): Promise<MovieActor[] | undefined> {
    const movies_actor = await this.ormRepository.find({
      where: {
        actor_id,
      },
      relations: ['actor', 'movie'],
    });

    return movies_actor;
  }

  public async findById(id: string): Promise<MovieActor | undefined> {
    const movie_actor = await this.ormRepository.findOne(id);

    return movie_actor;
  }

  public async create(data: ICreateMovieActorDTO): Promise<MovieActor> {
    const movie_actor = this.ormRepository.create(data);

    await this.ormRepository.save(movie_actor);

    return movie_actor;
  }

  public async save(movie_actor: MovieActor): Promise<MovieActor> {
    return this.ormRepository.save(movie_actor);
  }
}

export default MovieActorsRepository;
