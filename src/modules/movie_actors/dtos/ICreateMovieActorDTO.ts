export default interface ICreateMovieActorDTO {
  movie_id: string;
  actor_id: string;
  role: string;
}
