import ICreateDirectorDTO from '../dtos/ICreateDirectorDTO';
import Director from '../infra/typeorm/entities/Director';

export default interface IDirectorsRepository {
  findAll(): Promise<Director[] | undefined>;
  findById(id: string): Promise<Director | undefined>;
  findByName(name: string): Promise<Director | undefined>;
  create(data: ICreateDirectorDTO): Promise<Director>;
  save(director: Director): Promise<Director>;
}
