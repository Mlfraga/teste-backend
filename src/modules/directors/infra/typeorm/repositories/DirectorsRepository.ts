import { getRepository, Repository } from 'typeorm';

import ICreateDirectorDTO from '@modules/directors/dtos/ICreateDirectorDTO';
import IDirectorsRepository from '@modules/directors/repositories/IDirectorsRepository';

import Director from '../entities/Director';

class DirectorsRepository implements IDirectorsRepository {
  private ormRepository: Repository<Director>;

  constructor() {
    this.ormRepository = getRepository(Director);
  }

  public async findAll(): Promise<Director[] | undefined> {
    const directors = await this.ormRepository.find({});

    return directors;
  }

  public async findByName(name: string): Promise<Director | undefined> {
    const director = await this.ormRepository.findOne({
      where: {
        name,
      },
    });

    return director;
  }

  public async findById(id: string): Promise<Director | undefined> {
    const director = await this.ormRepository.findOne(id);

    return director;
  }

  public async create(data: ICreateDirectorDTO): Promise<Director> {
    const director = this.ormRepository.create(data);

    await this.ormRepository.save(director);

    return director;
  }

  public async save(director: Director): Promise<Director> {
    return this.ormRepository.save(director);
  }
}

export default DirectorsRepository;
