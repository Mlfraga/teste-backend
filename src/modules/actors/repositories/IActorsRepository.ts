import ICreateUserDTO from '../dtos/ICreateActorDTO';
import User from '../infra/typeorm/entities/Actor';

export default interface IActorsRepository {
  findAll(): Promise<User[] | undefined>;
  findById(id: string): Promise<User | undefined>;
  findByName(name: string): Promise<User | undefined>;
  create(data: ICreateUserDTO): Promise<User>;
  save(user: User): Promise<User>;
}
