import { getRepository, Repository } from 'typeorm';

import ICreateActorDTO from '@modules/actors/dtos/ICreateActorDTO';
import IActorsRepository from '@modules/actors/repositories/IActorsRepository';

import Actor from '../entities/Actor';

class ActorsRepository implements IActorsRepository {
  private ormRepository: Repository<Actor>;

  constructor() {
    this.ormRepository = getRepository(Actor);
  }

  public async findAll(): Promise<Actor[] | undefined> {
    const actors = await this.ormRepository.find({});

    return actors;
  }

  public async findByName(name: string): Promise<Actor | undefined> {
    const actor = await this.ormRepository.findOne({
      where: {
        name,
      },
    });

    return actor;
  }

  public async findById(id: string): Promise<Actor | undefined> {
    const actor = await this.ormRepository.findOne(id);

    return actor;
  }

  public async create(data: ICreateActorDTO): Promise<Actor> {
    const actor = this.ormRepository.create(data);

    await this.ormRepository.save(actor);

    return actor;
  }

  public async save(actor: Actor): Promise<Actor> {
    return this.ormRepository.save(actor);
  }
}

export default ActorsRepository;
