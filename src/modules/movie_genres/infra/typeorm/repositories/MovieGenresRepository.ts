import { getRepository, Repository } from 'typeorm';

import ICreateMovieGenreDTO from '@modules/movie_genres/dtos/ICreateMovieGenreDTO';
import IMovieGenresRepository from '@modules/movie_genres/repositories/IMovieGenresRepository';

import MovieGenre from '../entities/MovieGenre';

class MovieGenresRepository implements IMovieGenresRepository {
  private ormRepository: Repository<MovieGenre>;

  constructor() {
    this.ormRepository = getRepository(MovieGenre);
  }

  public async findAll(): Promise<MovieGenre[] | undefined> {
    const movie_genres = await this.ormRepository.find({
      relations: ['movie'],
    });

    return movie_genres;
  }

  public async findByMovieId(
    movie_id: string,
  ): Promise<MovieGenre[] | undefined> {
    const movie_genres = await this.ormRepository.find({
      where: {
        movie_id,
      },
      relations: ['movie'],
    });

    return movie_genres;
  }

  public async findById(id: string): Promise<MovieGenre | undefined> {
    const movie_genre = await this.ormRepository.findOne(id);

    return movie_genre;
  }

  public async create(data: ICreateMovieGenreDTO): Promise<MovieGenre> {
    const movie_genre = this.ormRepository.create(data);

    await this.ormRepository.save(movie_genre);

    return movie_genre;
  }

  public async save(movie_genre: MovieGenre): Promise<MovieGenre> {
    return this.ormRepository.save(movie_genre);
  }
}

export default MovieGenresRepository;
