import { Exclude } from 'class-transformer';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';

import Movie from '@modules/movies/infra/typeorm/entities/Movie';

@Entity('movie_genres')
export default class MovieGenre {
  @PrimaryGeneratedColumn('uuid')
  @Exclude()
  id: string;

  @Column()
  @Exclude()
  movie_id: string;

  @ManyToOne(() => Movie, movie => movie.genres)
  @JoinColumn({ name: 'movie_id' })
  movie: Movie;

  @Column()
  genre: string;

  @CreateDateColumn()
  @Exclude()
  created_at: Date;

  @CreateDateColumn()
  @Exclude()
  updated_at: Date;
}
