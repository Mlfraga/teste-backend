import ICreateMovieGenreDTO from '../dtos/ICreateMovieGenreDTO';
import MovieGenre from '../infra/typeorm/entities/MovieGenre';

export default interface IMovieGenresRepository {
  findAll(): Promise<MovieGenre[] | undefined>;
  findById(id: string): Promise<MovieGenre | undefined>;
  findByMovieId(movie_id: string): Promise<MovieGenre[] | undefined>;
  create(data: ICreateMovieGenreDTO): Promise<MovieGenre>;
  save(movie_genre: MovieGenre): Promise<MovieGenre>;
}
