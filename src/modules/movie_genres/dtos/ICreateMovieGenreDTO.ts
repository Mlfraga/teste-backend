export default interface ICreateMovieGenreDTO {
  movie_id: string;
  genre: string;
}
