# Instruções para executar o projeto

Siga esses passos para executar o projeto.

1. Clone esse repositório.
2. Execute o comando yarn / npm install para instalar as dependências.
3. Configure o arquivo docker-compose.example que está na raiz do projeto e o renomeie para  docker-compose.yml
3. Execute o comando docker-compose up -d ou crie o container docker manualmente de acordo com os dados do ormconfig.
4. Execute yarn dev para executar o projeto.
5. Use as collections do insomnia ou do postman que estão localizadas na raiz do projeto, para testar as requisições.

# Features complementares faltantes

- Implementação de testes automatizados com jest em todos os 'services'. (Até
configurei o jest e comecei a criar o teste do serviço de autenticação, mas não sobrou tempo)

- Gostaria de fazer uma openAPI caprichada.

- Gostaria de criar duas novas tabelas para armazenar fotos e vídeos dos filmes.
